  function getHeaderHeight(onlyNavbar) {
    var navbar =  document.querySelector('id').innerHeight() || 0;

    if (onlyNavbar) return navbar;

    return navbar + document.querySelector('id').innerHeight() || 0;
  }

  function getTopDistance() {
    return $(window).scrollTop();
  }

  function getWidth(){
   return $(window).innerWidth();
  }

  function getElementTopDistance(el) {
    if (!el.offset()) return;
    return el.offset().top - $(window).scrollTop();
  }

  function isVisible(el, onViewport) {
    return el.visible(onViewport);
  }